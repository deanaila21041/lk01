public class Kereta {
    private String jenisKereta;
    private int jumlahTiket;
    private Ticket[] tiket;

    // constructor kereta komuter
    public Kereta() {
        this.jenisKereta = "Komuter";
        this.jumlahTiket = 1000;
        tiket = new Ticket[this.jumlahTiket];
    }

    // constructor KAJJ (Kereta Api Jarak Jauh)
    public Kereta(String jenisKereta, int jumlahTiket) {
        this.jenisKereta = jenisKereta;
        this.jumlahTiket = jumlahTiket;
        tiket = new Ticket[this.jumlahTiket];
    }

    // method untuk menambah tiket kereta komuter
    public void tambahTiket(String nama) {
        if (jumlahTiket > 0) {
            this.jumlahTiket--;
            for (int i = 0; i < tiket.length; i++) {
                if (tiket[i] != null) {
                    continue;
                } else {
                    tiket[i] = new Ticket(nama);
                    break;
                }
            }
            System.out.println("======================================================================");
            System.out.print("Tiket berhasil dipesan. ");
            // sisa tiket yang tersedia akan ditampilkan jika jumlah tiket yang tersedia
            // kurang dari 30
            if (jumlahTiket < 30 && jumlahTiket >= 0) {
                System.out.print("Sisa tiket tersedia: " + jumlahTiket);
            }
            System.out.println();
        } else {
            System.out.println("======================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    // method untuk menambah tiket KAJJ
    public void tambahTiket(String nama, String asal, String tujuan) {
        if (jumlahTiket > 0) {
            this.jumlahTiket--;
            for (int i = 0; i < tiket.length; i++) {
                if (tiket[i] != null) {
                    continue;
                } else {
                    tiket[i] = new Ticket(nama, asal, tujuan);
                    break;
                }
            }
            System.out.println("======================================================================");
            System.out.print("Tiket berhasil dipesan. ");
            // sisa tiket yang tersedia akan ditampilkan jika jumlah tiket yang tersedia
            // kurang dari 30
            if (jumlahTiket < 30 && jumlahTiket >= 0) {
                System.out.print("Sisa tiket tersedia: " + jumlahTiket);
            }
            System.out.println();
        } else {
            System.out.println("======================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    // method untuk menampilkan pemesanan tiket kereta
    public void tampilkanTiket() {
        System.out.println("======================================================================");
        System.out.println("Daftar penumpang kereta api " + this.jenisKereta + ":");
        System.out.println("-------------------------------------");

        for (Ticket tiket : tiket) {
            if (tiket == null) {
                break;
            } else {
                tiket.printTicket();
            }
        }
    }
}