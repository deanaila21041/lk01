public class Ticket {
    private String nama;
    private String asal;
    private String tujuan;

    // constructor tiket kereta komuter
    public Ticket(String nama) {
        this.nama = nama;
    }

    // constructor tiket KAJJ (Kereta Api Jarak Jauh)
    public Ticket(String nama, String asal, String tujuan) {
        this.nama = nama;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    // method menampilkan detail pemesanan tiket online
    public void printTicket() {
        if (asal == null && tujuan == null) {
            System.out.println("Nama: " + this.nama);
        } else {
            System.out
                    .print("Nama: " + this.nama + "\n" + "Asal: " + this.asal + "\n" + "Tujuan: " + this.tujuan + "\n");
            System.out.println("-----------------------");
        }
    }
}